const process = require('process')
const mineflayer = require('mineflayer')
const config = require('./config.json')

let bot = mineflayer.createBot(config)

bot.on('error', (err) => console.log(err))

bot.once('login', () => {
    console.log('bot:login')
})

bot.once('spawn', () => {
    console.log('bot:spawn')

    setTimeout(() => {
        console.log('bot:chat:survival')
        bot.chat('/server survival')
    }, 60000)
})

bot.on('chat', (username, message) => {
    console.log(`bot:chat - ${username}: ${JSON.stringify(message)}`)
})

bot.on('kicked', (reason) => {
    console.log('bot:kicked', reason)

    process.exit()
})

bot.on('whisper', (username, message, translate, jsonMsg, matches) => {
    console.log('bot:whisper', username, message)

    if (username === 'BobDeBouwer1337') {
        if (message === 'summon') bot.chat('/tpa BobDeBouwer1337')
        else if (message === 'accept') bot.chat('/tpaccept')
        else if (message === 'survival') bot.chat('/server survival')
    }
})
