# iron-golem-idle

## Known bugs

- Mineflayer crash upon receiving invalid tablist data. Solution: try catch in "node_modules/mineflayer/lib/plugins/tablist.js" at the "playerlist_header" event.